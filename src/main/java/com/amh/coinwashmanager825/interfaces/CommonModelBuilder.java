package com.amh.coinwashmanager825.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
