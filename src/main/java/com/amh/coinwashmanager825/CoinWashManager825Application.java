package com.amh.coinwashmanager825;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoinWashManager825Application {

	public static void main(String[] args) {
		SpringApplication.run(CoinWashManager825Application.class, args);
	}

}
