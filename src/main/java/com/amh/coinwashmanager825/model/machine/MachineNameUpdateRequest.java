package com.amh.coinwashmanager825.model.machine;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MachineNameUpdateRequest {

    @NotNull
    @Length(max = 15)
    @ApiModelProperty(notes = "머신 이름")
    private String machineName;

}
