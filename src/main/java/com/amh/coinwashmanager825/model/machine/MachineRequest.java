package com.amh.coinwashmanager825.model.machine;

import com.amh.coinwashmanager825.enums.MachineType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MachineRequest {

    @NotNull
    @Enumerated(EnumType.STRING) //length 필요없음
    @ApiModelProperty(notes = "머신 타입")
    private MachineType machineType;

    @NotNull
    @Length(max = 15)
    @ApiModelProperty(notes = "머신 이름")
    private String machineName;

    @NotNull
    @ApiModelProperty(notes = "머신 구매일")
    private LocalDate datePurchase;

    @NotNull
    @ApiModelProperty(notes = "머신 가격")
    private Double machinePrice;

}
