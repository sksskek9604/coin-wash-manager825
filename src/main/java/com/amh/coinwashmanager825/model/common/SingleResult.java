package com.amh.coinwashmanager825.model.common;

import com.amh.coinwashmanager825.model.common.CommonResult;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SingleResult<T> extends CommonResult {
    private T data;
}
