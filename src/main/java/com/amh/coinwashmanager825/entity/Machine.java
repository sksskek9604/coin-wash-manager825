package com.amh.coinwashmanager825.entity;

import com.amh.coinwashmanager825.enums.MachineType;
import com.amh.coinwashmanager825.interfaces.CommonModelBuilder;
import com.amh.coinwashmanager825.model.machine.MachineNameUpdateRequest;
import com.amh.coinwashmanager825.model.machine.MachineRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Machine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private MachineType machineType;

    @Column(nullable = false, length = 15)
    private String machineName;

    @Column(nullable = false)
    private LocalDate datePurchase;

    @Column(nullable = false)
    private Double machinePrice;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putDataName(MachineNameUpdateRequest request) {
        this.machineName = request.getMachineName();
    }

    public void putMachine(MachineRequest request) {
        this.machineType = request.getMachineType();
        this.machineName = request.getMachineName();
        this.datePurchase = request.getDatePurchase();
        this.machinePrice = request.getMachinePrice();
        this.dateUpdate = LocalDateTime.now();
    }

    //2 빈 생성자
    private Machine(MachineBuilder machineBuilder) {
        this.machineType = machineBuilder.machineType;
        this.machineName = machineBuilder.machineName;
        this.datePurchase = machineBuilder.datePurchase;
        this.machinePrice = machineBuilder.machinePrice;
        this.dateCreate = machineBuilder.dateCreate;
        this.dateUpdate = machineBuilder.dateUpdate; // dateC/U는 빌더한테 받아와야함
    }

    // 1 빌더
    public static class MachineBuilder implements CommonModelBuilder<Machine> {

        private final MachineType machineType;
        private final String machineName;
        private final LocalDate datePurchase;
        private final Double machinePrice;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        // 3머신빌더에 대한 생성자
        public MachineBuilder(MachineRequest request) {
            this.machineType = request.getMachineType();
            this.machineName = request.getMachineName();
            this.datePurchase = request.getDatePurchase();
            this.machinePrice = request.getMachinePrice();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Machine build() {
            return new Machine(this);
        }
    }



}
