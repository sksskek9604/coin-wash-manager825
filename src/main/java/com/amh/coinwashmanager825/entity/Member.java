package com.amh.coinwashmanager825.entity;

import com.amh.coinwashmanager825.interfaces.CommonModelBuilder;
import com.amh.coinwashmanager825.model.member.MemberJoinRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String memberName;

    @Column(nullable = false, length = 20)
    private String memberPhone;

    @Column(nullable = false)
    private LocalDate birthday;

    @Column(nullable = false)
    private LocalDateTime dateJoin; //회원가입날짜

    @Column(nullable = false)
    private Boolean isEnable; // true 유효한지 / false '아이디 없습니다'뜨게함. 탈퇴한 회원인지
    // 탈퇴해도 이용내역은 보관해야함 매출내역, 회계내역이 달라지면 안되니까.

    private LocalDateTime dateWithdrawal; //회원 탈퇴날짜
    // 가입하자마자 탈퇴가 되진 않으니, 빌더 안에 필요할까요?
    // 즉, 데이터를 집어넣어주는 역할인 '빌더'에겐
    // 필요없기 때문에 넣어주지 않아도 됩니다.

    public void putWithdrawal() { //탈퇴했다~라고 해주는 것.
        this.isEnable = false;
        this.dateWithdrawal = LocalDateTime.now();
    }

    public void putMemberInfo(MemberJoinRequest request) {
        this.memberName = request.getMemberName();
        this.memberPhone = request.getMemberPhone();
        this.birthday = request.getBirthday();
    }

    private Member(MemberBuilder memberBuilder) {
        this.memberName = memberBuilder.memberName;
        this.memberPhone = memberBuilder.memberPhone;
        this.birthday = memberBuilder.birthday;
        this.dateJoin = memberBuilder.dateJoin;
        this.isEnable = memberBuilder.isEnable;

    }
    public static class MemberBuilder implements CommonModelBuilder<Member> {

        private final String memberName;
        private final String memberPhone;
        private final LocalDate birthday;
        private final LocalDateTime dateJoin; //회원가입날짜
        private final Boolean isEnable;

        public MemberBuilder(MemberJoinRequest joinRequest) {
            this.memberName = joinRequest.getMemberName();
            this.memberPhone = joinRequest.getMemberPhone();
            this.birthday = joinRequest.getBirthday();
            this.dateJoin = LocalDateTime.now();
            this.isEnable = true; // 가입하는 순간 가능이 되니까.

        }


        @Override
        public Member build() {
            return new Member(this);
        }
    }

}
