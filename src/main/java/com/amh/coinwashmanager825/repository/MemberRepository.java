package com.amh.coinwashmanager825.repository;

import com.amh.coinwashmanager825.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MemberRepository extends JpaRepository<Member, Long> {
    List<Member> findAllByIsEnableOrderByIdDesc(Boolean isEnable);
    // 전부다 찾지만, 그중에서 id를 최근순으로 가져오고 가능/불가능 여부 따지기
}
