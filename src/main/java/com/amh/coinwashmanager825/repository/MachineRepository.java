package com.amh.coinwashmanager825.repository;

import com.amh.coinwashmanager825.entity.Machine;
import com.amh.coinwashmanager825.enums.MachineType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineRepository extends JpaRepository<Machine, Long> {
    List<Machine> findAllByMachineTypeOrderByIdDesc(MachineType machineType);
}
