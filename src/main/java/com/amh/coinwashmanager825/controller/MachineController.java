package com.amh.coinwashmanager825.controller;

import com.amh.coinwashmanager825.enums.MachineType;
import com.amh.coinwashmanager825.model.common.CommonResult;
import com.amh.coinwashmanager825.model.common.ListResult;
import com.amh.coinwashmanager825.model.common.SingleResult;
import com.amh.coinwashmanager825.model.machine.MachineDetail;
import com.amh.coinwashmanager825.model.machine.MachineItem;
import com.amh.coinwashmanager825.model.machine.MachineNameUpdateRequest;
import com.amh.coinwashmanager825.model.machine.MachineRequest;
import com.amh.coinwashmanager825.service.MachineService;
import com.amh.coinwashmanager825.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "기계 관리 시스템")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/machine")
public class MachineController {
    private final MachineService machineService;

    @ApiOperation(value = "기계 정보 등록")
    @PostMapping("/new")
    public CommonResult setMachine(MachineRequest request) {
        machineService.setMachine(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기계 정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "기계 시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<MachineDetail> getMachine(@PathVariable long id) {
        return ResponseService.getSingleResult(machineService.getMachine(id));

    }
    // pathvariable은 무조건받아야하지만 지금은 두개를 합친 상황. 있어도, 없어도 된다라는 뜻의 연결 주석이 필요함.
    // 연결할 때 @RequestParam / required = false (@RequestParam의 기본값은 required = true이기 때문에 false의 경우를 써줘야함.)
    @ApiOperation(value = "기계 정보 리스트(기본/기계타입별)")
    @GetMapping("/search")
    public ListResult<MachineItem> getMachines(@RequestParam( value = "machineType", required = false) MachineType machineType) {
        if (machineType == null) {
            return ResponseService.getListResult(machineService.getMachines(), true);
        } else return ResponseService.getListResult(machineService.getMachines(machineType), true);
    }

    @ApiOperation(value = "기계 이름만 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "기계 시퀀스", required = true)
    })
    @PutMapping("/{id}")
    public CommonResult putMachineName(@PathVariable  long id, @RequestBody @Valid MachineNameUpdateRequest machineNameUpdateRequest) {
        machineService.putMachineName(id, machineNameUpdateRequest);
        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "기계 정보 전체수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "기계 시퀀스", required = true)
    })
    @PutMapping("/{id}")
    public CommonResult putMachine(@PathVariable long id, @RequestBody @Valid MachineRequest machineRequest) {
        machineService.putMachine(id, machineRequest);
    return ResponseService.getSuccessResult();

    }
}
