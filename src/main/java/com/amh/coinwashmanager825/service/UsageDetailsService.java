package com.amh.coinwashmanager825.service;

import com.amh.coinwashmanager825.entity.Machine;
import com.amh.coinwashmanager825.entity.Member;
import com.amh.coinwashmanager825.entity.UsageDetails;
import com.amh.coinwashmanager825.model.common.ListResult;
import com.amh.coinwashmanager825.model.UsageDetails.UsageDetailsItem;
import com.amh.coinwashmanager825.repository.UsageDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageDetailsService {

    private final UsageDetailsRepository usageDetailsRepository;

    public void setUsageDetails(Member member, Machine machine, LocalDateTime dateUsage) {
        UsageDetails usageDetails = new UsageDetails.UsageDetailsBuilder(machine, member, dateUsage).build();
        usageDetailsRepository.save(usageDetails);
    }

    // 고객에서 부터도 박스가 없고, 박스로 받을 수 없으니까 따로 따로 넣어줘야함.(시작일/종료일)
    public ListResult<UsageDetailsItem> getUsageDetailsAll(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(
                dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        ); // 0시 0분 0초 부터

        LocalDateTime dateEndTime = LocalDateTime.of(
                dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        ); // 23시 59분 59초

        List<UsageDetails> usageDetails = usageDetailsRepository.findAllByDateUsingGreaterThanEqualAndDateUsingLessThanEqualOrderByIdDesc(dateStartTime, dateEndTime);

        List<UsageDetailsItem> result = new LinkedList<>();

        usageDetails.forEach(usageDetails1 -> {
            UsageDetailsItem addItem = new UsageDetailsItem.UsageDetailsItemBuilder(usageDetails1).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }


}
