package com.amh.coinwashmanager825.service;

import com.amh.coinwashmanager825.entity.Machine;
import com.amh.coinwashmanager825.enums.MachineType;
import com.amh.coinwashmanager825.exception.CMissingDataException;
import com.amh.coinwashmanager825.model.common.ListResult;
import com.amh.coinwashmanager825.model.machine.MachineDetail;
import com.amh.coinwashmanager825.model.machine.MachineItem;
import com.amh.coinwashmanager825.model.machine.MachineNameUpdateRequest;
import com.amh.coinwashmanager825.model.machine.MachineRequest;
import com.amh.coinwashmanager825.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {
    private final MachineRepository machineRepository;

    public Machine getMachineData(long id) { //UsageDetails에서 사용하기 위한
        Machine machine= machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        return machine;
    } //usage쪽을 위한 기능 추가

    public void setMachine(MachineRequest request) {
        Machine machine = new Machine.MachineBuilder(request).build();
        machineRepository.save(machine);
    }

    public MachineDetail getMachine(long id) { //단수 get
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MachineDetail.MachineDetailBuilder(machine).build();
    }

    public ListResult<MachineItem> getMachines() { //복수 get 1
        List<Machine> machines = machineRepository.findAll();

        List<MachineItem> result = new LinkedList<>();

        for (Machine machine : machines) {
            MachineItem addItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(addItem);
        }

        machines.forEach(machine -> {
            MachineItem addItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
        // 위에는 for문 아래에는 forEach를 썼습니다.
        // 코드모양이 더 예쁜건 어느쪽인가요? 아래쪽이 좀 더 명확한 코드이기에 예쁘다라고 볼 수 있어요.
        // 리스트들(machines)을 하나(each)씩 던져줘라(forEach), 그걸 machine이라고 부를게
        // 위에꺼보단 점점 아래껄로 바꿔가야해요.

        // forEach 쓰는 법
        // 쓰는 법: 1. 원본의 리스트 이름을 갖고와(리스트 변수 machines)
        // 2. 점을 찍고 forEach()를 가져와
        // 3. 한뭉텅이를 뭐라고 부를거냐, 헷갈리지 않게 machine이라고 부를게 하고 -> {} 모양도 같이 눌러줘요.
        // 4. 위 for문에서 두줄이었으니까, 길어지니 중괄호가 필요한거예요. 중괄호 안에서 enter!
        // 5. for문 안에 썼던 내용 그대로 써주면 됩니다. -끝-

    }
    public ListResult<MachineItem> getMachines(MachineType machineType) { //복수 get 지정 리스트
        List<Machine> machines = machineRepository.findAllByMachineTypeOrderByIdDesc(machineType);

        List<MachineItem> result = new LinkedList<>();

        machines.forEach(machine -> {
            MachineItem addItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public void putMachineName(long id, MachineNameUpdateRequest request) { //이름만 바꾸기
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        machine.putDataName(request);
        machineRepository.save(machine);
    }

    public void putMachine(long id, MachineRequest request) { //전체 다 바꾸기
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        machine.putMachine(request);
        machineRepository.save(machine);
    }


}
