package com.amh.coinwashmanager825.service;

import com.amh.coinwashmanager825.entity.Member;
import com.amh.coinwashmanager825.exception.CMissingDataException;
import com.amh.coinwashmanager825.exception.CNoMemberDataException;
import com.amh.coinwashmanager825.model.common.ListResult;
import com.amh.coinwashmanager825.model.member.MemberItem;
import com.amh.coinwashmanager825.model.member.MemberJoinRequest;
import com.amh.coinwashmanager825.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getMemberData(long id) { //UsageDetails에서 사용하기 위한
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        return member;
    } // member에 색깔나오는 이유는 합칠 수 있어서.

    public void setMember(MemberJoinRequest joinRequest) {
        Member member = new Member.MemberBuilder(joinRequest).build();
        memberRepository.save(member);
    }
    public MemberItem getMemberOne(long id) { // 뭉텅이로 가지고 오는 기능
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MemberItem.MemberItemBuilder(member).build();
    }

    public ListResult<MemberItem> getMembersAll() { // 해당 항목 리스트 전체 다 가져오는 기능
        // 예시로 관리자가 필요해서?
        List<Member> members = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public ListResult<MemberItem> getMembersEnable(boolean isEnable) { // 전체 중에 'isEnable' 기준으로
        // 회원이면 보여주는 것 기본값이 TRUE
        // FALSE인 사람들 보여줘! 라는걸 스웨거에서 골라서 치면 나올것...(예상)
        List<Member> members = memberRepository.findAllByIsEnableOrderByIdDesc(isEnable);
        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    //코드는 똑같지만 개념이 생소한 부분이다. 그렇기 때문에 '흐름'에 집중을 하면 된다.

    // '탈퇴'란건 탈퇴 한 사람이 있고, 그 사람이 또 탈퇴가 가능하진 않다는 점.
    // 탈퇴할 때 아이디만 필요하다는 가정하에 진행해볼게요.
    public void putMemberWithdrawal(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        //활성화 상태 체크, 탈퇴했는지 안했는지 알아야함. 메세지 조작을 어떻게 해야하냐면..

        // 참의 내용이 원랜 들어가지만, false일때가 참이고 싶으면 not을 붙이는게 !로 가능
        if(!member.getIsEnable()) throw new CNoMemberDataException();

        member.putWithdrawal();
        memberRepository.save(member);
    }
    public void putMemberInfo(long id, MemberJoinRequest request) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.putMemberInfo(request);
    }
}
