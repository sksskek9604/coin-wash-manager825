package com.amh.coinwashmanager825.exception;

public class CNoMemberDataException extends RuntimeException { //ResultCode에 만들었으면, exception도 하나 같이 만들어줘야함.

    // 1번, 생성자 3개만들 것임
    public CNoMemberDataException(String msg, Throwable t) {
        super(msg, t); //위에 두개 받아줄 것.
    }

    public CNoMemberDataException(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CNoMemberDataException() {
        super();
    }
}
